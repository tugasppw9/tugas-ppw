from django.conf.urls import url
from .views import index, add_todo, delete_status
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
    url(r'^delete_status/(?P<id_status>\d+)/$', delete_status, name = 'delete_status'),
]
