from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Form_status
from .models import Todo
from halaman_profile.views import profil
# Create your views here.
response = {}
def index(request):
	response['author'] = "Cut Syifa Salvira" #TODO Implement yourname
	response['profile_name'] = profil.name
	todo = Todo.objects.order_by('-id')
	response['todo'] = todo
	html = 'update_status/update_status.html'
	response['todo_form'] = Form_status
	return render(request, html, response)

def add_todo(request):
	form = Form_status(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['description'] = request.POST['description']
		todo = Todo(description=response['description'])
		todo.save()
		return HttpResponseRedirect('/update-status/')
	else:
		return HttpResponseRedirect('/update-status/')

def delete_status(request, id_status):
    status = Todo.objects.get(pk = id_status)
    status.delete()
    return HttpResponseRedirect('/update-status/')

