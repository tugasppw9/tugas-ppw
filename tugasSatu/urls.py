"""tugasSatu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import update_status.urls as update_status
import add_friends.urls as add_friends
from django.views.generic.base import RedirectView
import halaman_profile.urls as halaman_profile
import menu_stats.urls as menu_stats

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^update-status/', include(update_status,namespace='update-status')),
    url(r'^add-friends/',include(add_friends,namespace='add-friends')),
    url(r'^$', RedirectView.as_view(url='/update-status/', permanent = True), name = 'index'),
    url(r'^halaman-profile/', include(halaman_profile, namespace ='halaman-profile')),
    url(r'^menu-stats/', include(menu_stats, namespace ='menu-stats')),
]
