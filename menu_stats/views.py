from django.shortcuts import render
from django.http import HttpResponseRedirect
from add_friends.models import Add
from update_status.models import Todo
from halaman_profile.views import profile_name

# Create your views here.
response = {}
def index(request):    
    response['author'] = "Rebecca" #TODO Implement yourname
    
    bnyk_temen = Add.objects.all().count()
    response['temen']= bnyk_temen 

    bnyk_status = Todo.objects.all().count()
    response['status'] = bnyk_status

    nama_profile = profile_name
    response['namaP'] = nama_profile

    if bnyk_status > 0:
        response['lastT'] = Todo.objects.last().created_date
        response['lastP'] = Todo.objects.last().description
    else:
        response['lastT'] = "__-__"
        response['lastP'] = "no post"

    html = 'menu_stats/menu_stats.html'
    
    return render(request, html, response)

