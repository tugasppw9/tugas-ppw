from django.test import TestCase
from django.test import Client
from .views import index
from django.urls import resolve
from update_status.models import Todo
from halaman_profile.views import profile_name


class MenuStatsUnitTest(TestCase):

    # cek apakah url ada
	def test_menu_stats_is_exist(self):
		response = Client().get('/menu-stats/')
		self.assertEqual(response.status_code, 200)

	def test_menu_stats_using_index_func(self):
		found = resolve('/menu-stats/')
		self.assertEqual(found.func, index)