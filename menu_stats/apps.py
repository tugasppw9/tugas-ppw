from django.apps import AppConfig


class MenuStatsConfig(AppConfig):
    name = 'menu_stats'
