from django.shortcuts import render
from datetime import datetime, date
from .models import Profiledata

# Create your views here.
profile_name = 'Gigi Hadid' #TODO implement your name here
birth_date = date(1995,4,23) #TODO implement your birthday
birthdate = birth_date.strftime('%d %B %Y')
gender = 'Female' #TODO implement your gender
email = 'gigihadid@gmail.com' #TODO implement your email
desc_profile = "victoria's secret model lalala lilili yeyeye"
#TODO implement your expertise minimal 3
expert = ["fashion model", "actress", "wawww"]

profil = Profiledata(name = profile_name, Birthday = birthdate, Gender= gender, Email = email, Description=desc_profile, expertise=expert)

def index(request):
	#response['author'] = "Redhita Putri"
	#response['Name'] = Profile.Name
	#response['Birthday'] = Profile.Birthday
	#response['Gender'] = Profile.Gender
	#response['expertise'] = Profile.expertise
	#response['Email'] = Profile.Email
	#response['Description'] = Profile.Description
	response = {'name' : profil.name, 'Birthday' : profil.Birthday, 'Gender': profil.Gender, 'expertise': profil.expertise, 'Description' : profil.Description, 'Email': profil.Email}
	html = 'halaman_profile/halaman_profile.html'
	return render(request, html, response)	