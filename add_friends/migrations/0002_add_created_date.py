# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-08 06:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('add_friends', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='add',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
