from django.shortcuts import render
from .models import Add
from .forms import AddFriends_Form
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
def index(request): #cuma nampillin halaman add-friends
	response['author'] = "Rani Lasma Uli"
	response['friends'] = Add.objects.order_by('-id')
	response['add_friends_form'] = AddFriends_Form
	html = 'add_friends/add_friends.html'
	return render(request,html,response)

def add(request):
    form = AddFriends_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        add = Add(name=response['name'],url=response['url'])
        add.save()
        return HttpResponseRedirect('/add-friends/')
    else:
        return HttpResponseRedirect('/add-friends/')


