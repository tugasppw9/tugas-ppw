from django.db import models

# Create your models here.
class Add(models.Model):
	name = models.CharField(max_length=30)
	url = models.URLField(max_length=100)
	created_date = models.DateTimeField(auto_now_add=True)