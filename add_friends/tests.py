from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Add
from .forms import AddFriends_Form
# Create your tests here.
class addFriendsUnitTest(TestCase):
	def test_add_friends_url_is_exist(self):
		response = Client().get('/add-friends/')
		self.assertEqual(response.status_code, 200)
	
	def test_lab5_using_index_func(self):
		found = resolve('/add-friends/')
		self.assertEqual(found.func, index)
	
	def test_model_can_create_new_friends(self):
            # Creating a new activity
		new_friends = Add.objects.create(name='Suponjibobi', url='www.suponjibobi.co.id')

            # Retrieving all available activity
		counting_all_available_friends = Add.objects.all().count()
		self.assertEqual(counting_all_available_friends, 1)

	def test_AddFriends_Form_has_placeholder_and_css_classes(self):
		form = AddFriends_Form()
		self.assertIn('class="input_name', form.as_p())
		#self.assertIn('id="id_title"', form.as_p())
		self.assertIn('class="input_url', form.as_p())
		#self.assertIn('id="id_description', form.as_p())
	
	def test_form_validation_for_blank_items(self):
		form = AddFriends_Form(data={'name': '', 'url': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['name'],
			["This field is required."]
			),

		self.assertEqual(
			form.errors['url'],
			["This field is required."]
			)

	def test_AddFriends_post_success_and_render_the_result(self):
		test = 'Anonymous'
		#menambah list teman baru tengan nama = "Anonymus" dan url = "Anonymus"
		response_post = Client().post('/add-friends/add', {'name': test, 'url': 'http://blabla.com'})
		self.assertEqual(response_post.status_code, 302)

		#mengecek apakah penambahan teman baru tadi sudah di-post di html
		response= Client().get('/add-friends/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)




