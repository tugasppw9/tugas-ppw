from django import forms

class AddFriends_Form(forms.Form):
	error_messages = {
		'required' : "Harap mengisi input ini",
	}
	name_attributes = {
	'type' : 'text',
	'class' : 'input_name',
	'placeholder' : "Input your friend's name here...",
	}
	url_attributes = {
	'type' : 'text',
	'class' : 'input_url',
	'rows' : 2,
	'cols' : 50,
	'placeholder' : "Input your friend's URL here...",
	}

	name = forms.CharField(label='Name',required=True,max_length=30,widget=forms.TextInput(attrs=name_attributes))

	url = forms.URLField(label='URL',required=True,widget=forms.TextInput(attrs=url_attributes))